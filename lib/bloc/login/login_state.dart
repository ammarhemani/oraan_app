import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class LoginState extends Equatable {
  final bool isFormValid;
  final bool autoValidate;
  final bool obscurePassword;
  final bool rememberMe;
  final bool isLoading;
  final bool isSuccess;
  final bool isFailure;
  final int userId;
  final String developerMessage;

  const LoginState({
    @required this.isFormValid,
    @required this.autoValidate,
    @required this.obscurePassword,
    @required this.rememberMe,
    @required this.isLoading,
    @required this.isSuccess,
    @required this.isFailure,
    @required this.userId,
    @required this.developerMessage,
  });

  factory LoginState.initial() {
    return const LoginState(
      isFormValid: false,
      autoValidate: false,
      obscurePassword: true,
      rememberMe: false,
      isLoading: false,
      isSuccess: false,
      isFailure: false,
      userId: 0,
      developerMessage:
          "Something happened that shouldn't have happened. Please submit a bug report so we can have a look.",
    );
  }

  factory LoginState.success() {
    return const LoginState(
      isFormValid: true,
      rememberMe: true,
      autoValidate: true,
      userId: 0,
      developerMessage: 'Success',
      isLoading: false,
      isSuccess: true,
      isFailure: false,
      obscurePassword: true,
    );
  }

  factory LoginState.failure({String error = "Oops, something wen't wrong.."}) {
    return LoginState(
      isFormValid: true,
      rememberMe: false,
      autoValidate: true,
      userId: 0,
      developerMessage: error ?? "Oops, something wen't wrong..",
      isLoading: false,
      isSuccess: false,
      isFailure: true,
      obscurePassword: true,
    );
  }

  LoginState copyWith({
    bool isFormValid,
    bool autoValidate,
    bool obscurePassword,
    bool rememberMe,
    bool isLoading,
    int userId,
    String developerMessage,
  }) {
    return LoginState(
      isFormValid: isFormValid ?? this.isFormValid,
      autoValidate: autoValidate ?? this.autoValidate,
      obscurePassword: obscurePassword ?? this.obscurePassword,
      rememberMe: rememberMe ?? this.rememberMe,
      isLoading: isLoading ?? this.isLoading,
      isSuccess: false,
      isFailure: false,
      userId: userId ?? this.userId,
      developerMessage: developerMessage ?? this.developerMessage,
    );
  }

  @override
  List<Object> get props => [
        isFormValid,
        autoValidate,
        obscurePassword,
        rememberMe,
        isLoading,
        isSuccess,
        isFailure,
        userId,
      ];
}
