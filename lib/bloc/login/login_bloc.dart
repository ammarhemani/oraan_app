import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:oraan_test_app/services/helper_classes/network_provider.dart';
import 'package:oraan_test_app/services/helper_classes/shared_prefs_helper.dart';
import 'login.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc();
  @override
  LoginState get initialState => LoginState.initial();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is AutoValidate) {
      yield state.copyWith(autoValidate: true);
    }
    if (event is ObscurePasswordToggled) {
      yield state.copyWith(obscurePassword: !event.value);
    }
    if (event is RememberMeToggled) {
      yield state.copyWith(rememberMe: event.rememberMe);
    }
    if (event is FormSubmitted) {
      yield state.copyWith(isLoading: true);
      try {
        await NetworkProvider.loginUser(event);
        await SharedPrefsHelper.changeLoggedInValue(newValue: true);
        await SharedPrefsHelper.setUserIdInSharedPrefs();
        yield LoginState.success();
      } on DioError catch (e) {
        yield LoginState.failure(error: e.error);
      } catch (e) {
        yield LoginState.failure(error: e.toString());
      }
    }
    if (event is FormReset) {
      yield LoginState.initial();
    }
  }
}
