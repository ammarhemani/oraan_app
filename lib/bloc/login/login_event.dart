import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class AutoValidate extends LoginEvent {
  final bool autoValidate;

  const AutoValidate({@required this.autoValidate});

  @override
  List<Object> get props => [autoValidate];
}

class RememberMeToggled extends LoginEvent {
  final bool rememberMe;

  const RememberMeToggled({@required this.rememberMe});

  @override
  List<Object> get props => [rememberMe];
}

class ObscurePasswordToggled extends LoginEvent {
  final bool value;

  const ObscurePasswordToggled({@required this.value});

  @override
  List<Object> get props => [value];
}

class FormSubmitted extends LoginEvent {
  final String phoneNumber;
  final String password;

  const FormSubmitted({
    @required this.phoneNumber,
    @required this.password,
  });
  @override
  List<Object> get props => [phoneNumber, password];
}

class FormReset extends LoginEvent {
  @override
  List<Object> get props => [];
}
