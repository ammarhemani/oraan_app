import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:oraan_test_app/services/helper_classes/network_provider.dart';
import 'home.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc();

  @override
  HomeState get initialState => HomeInitial();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is FetchLifeTimeSavings) {
      yield HomeLoading();
      try {
        final double savings = await NetworkProvider.fetchLifeTimeSavings();
        yield HomeLoaded(savings);
      } on DioError catch (e) {
        print(e);
        yield HomeError(developerMessage: e.error);
      }
    }
  }
}
