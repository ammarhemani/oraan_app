import 'package:meta/meta.dart';
import 'package:oraan_test_app/models/app_model.dart';
import 'package:oraan_test_app/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefsHelper {
  static const String loggedInPref = 'loggedIn';
  static const String firstLaunchPref = 'firstLaunch';
  static const String userId = 'userId';

  static Future<void> appLaunchInitialize() async {
    await setFirstLaunchValueInAppModel();
    await setLoggedInValueInAppModel();
  }

  static Future<void> setFirstLaunchValueInAppModel() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(firstLaunchPref) ?? true) {
      await prefs.setBool(firstLaunchPref, false);
      AppModel.instance.firstLaunch = true;
    } else {
      AppModel.instance.firstLaunch = false;
    }
  }

  static Future<void> setLoggedInValueInAppModel() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getBool(loggedInPref) ?? false) {
      AppModel.instance.loggedIn = true;
      User.instance.id = prefs.getInt(userId);
    } else {
      await prefs.setBool(loggedInPref, false);
      AppModel.instance.loggedIn = false;
    }
  }

  static Future<void> changeLoggedInValue({@required bool newValue}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(loggedInPref, newValue);
    AppModel.instance.loggedIn = newValue;
  }

  static Future<void> setUserIdInSharedPrefs() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(userId, User.instance.id);
  }

  static void clearUserValuesInMemory() {
    User.instance.clear();
  }
}
