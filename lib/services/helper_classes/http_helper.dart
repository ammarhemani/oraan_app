import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import '../../constants.dart';

class HttpHelper {
  final String baseUrl;
  final Dio dio = Dio();

  HttpHelper({@required this.baseUrl}) {
    initializeDio();
  }

  Future<Response> post(String endpoint, [FormData data]) async {
    return dio.post(
      endpoint,
      data: data,
    );
  }

  Future<Response> postRaw(
      String endpoint, Map<String, dynamic> rawData) async {
    return dio.post(
      endpoint,
      data: jsonEncode(rawData),
    );
  }

  Future<Response> get(String endpoint) async {
    return dio.get(endpoint);
  }

  void initializeDio() {
    dio.options.baseUrl = baseUrl;
    dio.interceptors.add(
      InterceptorsWrapper(
        onError: (DioError e) async {
          print('status code: ${e?.response?.statusCode}');
          print('response: ${e?.response?.data}');
          switch (e.type) {
            case DioErrorType.DEFAULT:
              e.error = kErrorStringConnection;
              return e;
              break;
            case DioErrorType.RESPONSE:
              final int _statusCode = e.response.statusCode;
              if (_statusCode == 404) {
                e.error = kErrorString404;
                return e;
              } else if (_statusCode == 502) {
                e.error = kErrorString502;
              }
              break;
            default:
              return e.error = kErrorStringGeneric;
          }
          return e;
        },
      ),
    );
  }
}
