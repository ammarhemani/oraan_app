import 'package:dio/dio.dart';
import 'package:oraan_test_app/bloc/login/login.dart';
import 'package:oraan_test_app/models/user.dart';
import '../../constants.dart';
import 'http_helper.dart';

///Low-level class used only by RepositoryBundle for data from network calls
class NetworkProvider {
  static final HttpHelper httpHelper = HttpHelper(baseUrl: kBaseUrl);

  ///Authentication: Login, register, reset pw, forget email
  static Future<User> loginUser(FormSubmitted event) async {
    const String _endpoint = '/users/login';
    final Map<String, dynamic> _rawData = {
      "userPhone": event.phoneNumber,
      "userPassword": event.password,
    };
    final Response _response = await httpHelper.postRaw(_endpoint, _rawData);
    final int userId = _response.data["data"]["userId"] as int;
    if (userId != null) {
      final User _user = User.instance.fromJson(_response.data);
      return _user;
    } else {
      throw DioError(error: 'Invalid credentials, please try again..');
    }
  }

  static Future<double> fetchLifeTimeSavings() async {
    final String _endpoint =
        '/installment/get-by-userid?user_id=${User.instance.id}';
    final Response _response = await httpHelper.get(_endpoint);
    print(_response);
    final double lifeTimeSavings =
        double.tryParse(_response.data["data"]["lifeTimeSavings"] ?? "");
    if (lifeTimeSavings != null) {
      return lifeTimeSavings;
    } else {
      print('error');
      throw DioError(error: "Oops, something went wrong..");
    }
  }
}
