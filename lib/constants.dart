import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

///App Config
const String kOrgName = 'Oraan';
const String kBaseUrl = 'https://naya-oraan.herokuapp.com/';

///Colors, Gradients, Shadows, BorderRadius etc
const Color kPrimaryColor = Color(0xFF4FC4CC);
const Color kSecondaryColor = Color(0xFF2B4656);

final List<BoxShadow> kContainerBoxShadow = [
  BoxShadow(color: kOutlineColor, blurRadius: 10, offset: Offset(0.0, 1.0))
];
final Color kOutlineColor = Colors.grey[300];
final BorderRadius kButtonBorderRadius = BorderRadius.circular(35);

///App Theme: textThemes, fonts, etc
final ThemeData kThemeData = ThemeData(
    primaryColor: kPrimaryColor,
    backgroundColor: Colors.grey[100],
    fontFamily: 'Montserrat',
    buttonTheme: ButtonThemeData().copyWith(
      buttonColor: kPrimaryColor,
      textTheme: ButtonTextTheme.primary,
    ),
    cupertinoOverrideTheme:
        CupertinoThemeData().copyWith(brightness: Brightness.dark),
    textTheme: TextTheme(
      headline1: TextStyle(
          color: Colors.black, fontWeight: FontWeight.w500, fontSize: 28),
      headline2: TextStyle(
          color: Colors.black, fontWeight: FontWeight.w500, fontSize: 18),
      headline3: TextStyle(
          color: Colors.grey, fontWeight: FontWeight.w500, fontSize: 16),
      headline5: TextStyle(
          color: Colors.black, fontWeight: FontWeight.w600, fontSize: 18),
      headline6: TextStyle(
          color: Colors.white, fontWeight: FontWeight.w600, fontSize: 18),
      subtitle1: TextStyle(color: Colors.black54, fontSize: 15),
      caption: TextStyle(
          color: Colors.black, fontSize: 12, fontWeight: FontWeight.w500),
    ));

///Error Message Strings
const String kErrorStringConnection =
    'Oops, something went wrong, please make sure you have a working internet connection..';
const String kErrorString404 =
    "Oops, looks like we can't find what you're looking for..";
const String kErrorString502 =
    'It seems the requested server is down, please try again in a while..';
const String kErrorStringGeneric = "Oops, something wen't wrong..";
