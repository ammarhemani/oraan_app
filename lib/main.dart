import 'package:flutter/material.dart';
import 'constants.dart';
import 'models/app_model.dart';
import 'screens/auth/login_screen.dart';
import 'screens/nav_bar_handler.dart';
import 'services/helper_classes/shared_prefs_helper.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefsHelper.appLaunchInitialize();
//  await SharedPrefsHelper.changeLoggedInValue(newValue: false);
  runApp(OraanApp());
}

class OraanApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: kOrgName,
      theme: kThemeData,
      initialRoute:
          AppModel.instance.loggedIn ? NavBarHandler.id : LoginScreen.id,

      ///routes with bottom navigation bar listed in each individual tab navigator class
      ///routes without bottom navigation bar listed below
      routes: {
        LoginScreen.id: (context) => LoginScreen(),
        NavBarHandler.id: (context) => NavBarHandler(),
      },
    );
  }
}
