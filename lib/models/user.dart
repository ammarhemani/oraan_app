class User {
  static User instance = User._internal();

  int id;

  User._internal();

  User fromJson(Map<String, dynamic> json) {
    id = json["data"]["userId"] as int;
    return instance;
  }

  void clear() {
    instance = User._internal();
  }
}
