import 'package:flutter/material.dart';
import 'package:oraan_test_app/components/no_data_found_widget.dart';

class TempScreen extends StatefulWidget {
  static const String id = 'temp_screen';
  @override
  _TempScreenState createState() => _TempScreenState();
}

class _TempScreenState extends State<TempScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: NoDataFoundWidget()),
    );
  }
}
