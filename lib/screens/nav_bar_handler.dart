import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:oraan_test_app/components/bottom_nav_bar/custom_nav_bar.dart';
import 'package:oraan_test_app/components/bottom_nav_bar/custom_nav_bar_item.dart';
import 'package:oraan_test_app/screens/committees/committees_tab_navigator.dart';
import 'package:oraan_test_app/screens/pay_now/pay_now_tab_navigator.dart';
import 'package:oraan_test_app/screens/settings/settings_tab_navigator.dart';
import 'package:oraan_test_app/screens/transactions/transactions_tab_navigator.dart';
import '../constants.dart';
import 'home/home_tab_navigator.dart';

class NavBarHandler extends StatefulWidget {
  static const id = 'nav_bar_handler';
  @override
  _NavBarHandlerState createState() => _NavBarHandlerState();
}

class _NavBarHandlerState extends State<NavBarHandler> {
  int currentIndex = 0;
  List<GlobalKey<NavigatorState>> navigatorKeys;
  final _homeTabKey = GlobalKey<NavigatorState>();
  final _committeesTabKey = GlobalKey<NavigatorState>();
  final _payNowTabKey = GlobalKey<NavigatorState>();
  final _transactionsTabKey = GlobalKey<NavigatorState>();
  final _settingsTabKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    navigatorKeys = [
      _homeTabKey,
      _committeesTabKey,
      _payNowTabKey,
      _transactionsTabKey,
      _settingsTabKey,
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness:
            currentIndex == 0 ? Brightness.dark : Brightness.light));
    return Scaffold(
      bottomNavigationBar: CustomNavBar(
        onTap: (int index) {
          setState(() => _handleTabTap(index));
          return null;
        },
        backgroundColor: Colors.white,
        selectedItemColor: kSecondaryColor,
        unselectedItemColor: kPrimaryColor,
        selectedBackgroundColor: Colors.transparent,
        currentIndex: currentIndex,
        items: [
          CustomNavBarItem(icon: Icons.home, title: 'Home'),
          CustomNavBarItem(
              icon: Icons.supervised_user_circle, title: 'Committees'),
          CustomNavBarItem(icon: Icons.add_circle_outline, title: 'Pay Now'),
          CustomNavBarItem(icon: Icons.notifications, title: 'Transactions'),
          CustomNavBarItem(
              icon: Icons.settings, title: 'Settings', badgeCount: 1)
        ],
      ),
      body: WillPopScope(
        onWillPop: _handleTabPop,
        child: IndexedStack(
          index: currentIndex,
          children: <Widget>[
            HomeTabNavigator(_homeTabKey),
            CommitteesTabNavigator(_committeesTabKey),
            PayNowTabNavigator(_payNowTabKey),
            TransactionsTabNavigator(_transactionsTabKey),
            SettingsTabNavigator(_settingsTabKey),
          ],
        ),
      ),
    );
  }

  void _handleTabTap(int index) {
    if (currentIndex == index) {
      if (navigatorKeys[currentIndex].currentState?.canPop() == true) {
        navigatorKeys[currentIndex]
            .currentState
            .popUntil((route) => route.isFirst);
      }
    } else {
      currentIndex = index;
    }
  }

  Future<bool> _handleTabPop() {
    if (navigatorKeys[currentIndex].currentState.canPop()) {
      navigatorKeys[currentIndex].currentState.pop();
    }
    return Future.value(false);
  }
}
