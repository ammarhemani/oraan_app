import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oraan_test_app/bloc/login/login.dart';
import 'package:oraan_test_app/components/auth/password_text_field.dart';
import 'package:oraan_test_app/components/auth/phone_number_text_field.dart';
import 'package:oraan_test_app/services/helper_classes/snack_bar_helper.dart';
import '../../constants.dart';
import '../nav_bar_handler.dart';

class LoginScreen extends StatefulWidget {
  static const String id = 'login_screen';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final LoginBloc _loginBloc = LoginBloc();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    return Scaffold(
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () => FocusScope.of(context).unfocus(),
        child: BlocConsumer<LoginBloc, LoginState>(
            bloc: _loginBloc,
            listener: (context, LoginState state) {
              if (state.isSuccess) {
                Navigator.pushNamedAndRemoveUntil(
                    context, NavBarHandler.id, (route) => false);
              }
              if (state.isFailure) {
                SnackBarHelper.showWarningSnackBar(
                    context, state.developerMessage);
              }
            },
            builder: (context, LoginState state) {
              return AbsorbPointer(
                absorbing: state.isLoading,
                child: SafeArea(
                  child: Form(
                    key: _formKey,
                    autovalidate: state.autoValidate,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Welcome back.',
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 22,
                              color: kSecondaryColor,
                            ),
                          ),
                          SizedBox(height: 30),
                          PhoneNumberTextField(
                            controller: _phoneNumberController,
                            autoValidate: state.autoValidate,
                          ),
                          SizedBox(height: 30),
                          PasswordTextField(
                            controller: _passwordController,
                            autoValidate: state.autoValidate,
                            obscurePassword: state.obscurePassword,
                            onShowPasswordIconTap: () => _loginBloc.add(
                                ObscurePasswordToggled(
                                    value: state.obscurePassword)),
                          ),
                          SizedBox(height: 30),
                          RaisedButton(
                            onPressed: _onSubmitPressed,
                            shape: RoundedRectangleBorder(
                                borderRadius: kButtonBorderRadius),
                            child: state.isLoading
                                ? CupertinoActivityIndicator()
                                : Text('LOGIN',
                                    style: TextStyle(color: Colors.white)),
                          ),
                          SizedBox(height: 30),
                          Text(
                            'FORGOT PASSCODE?',
                            style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.w500,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }

  void _onSubmitPressed() {
    if (_formKey.currentState.validate()) {
      _loginBloc.add(
        FormSubmitted(
          phoneNumber: _phoneNumberController.text,
          password: _passwordController.text,
        ),
      );
    } else {
      _loginBloc.add(const AutoValidate(autoValidate: true));
    }
  }

  @override
  void dispose() {
    _phoneNumberController.dispose();
    _passwordController.dispose();
    _loginBloc.close();
    super.dispose();
  }
}
