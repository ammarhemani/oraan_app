import 'package:flutter/material.dart';
import 'package:oraan_test_app/screens/temp_screen.dart';

class TransactionsTabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> _transactionsTabKey;
  const TransactionsTabNavigator(this._transactionsTabKey);
  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _transactionsTabKey,
      initialRoute: TempScreen.id,
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case TempScreen.id:
            builder = (BuildContext _) => TempScreen();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
