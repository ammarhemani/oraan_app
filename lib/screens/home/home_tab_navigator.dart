import 'package:flutter/material.dart';
import 'home_screen.dart';

class HomeTabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> _homeTabKey;
  const HomeTabNavigator(this._homeTabKey);
  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _homeTabKey,
      initialRoute: HomeScreen.id,
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case HomeScreen.id:
            builder = (BuildContext _) => HomeScreen();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
