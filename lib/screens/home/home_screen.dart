import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oraan_test_app/bloc/home/home.dart';
import 'package:oraan_test_app/screens/auth/login_screen.dart';
import 'package:oraan_test_app/services/helper_classes/shared_prefs_helper.dart';
import 'package:oraan_test_app/services/helper_classes/snack_bar_helper.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../../constants.dart';

class HomeScreen extends StatefulWidget {
  static const String id = 'home_screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final RefreshController _refreshController = RefreshController();
  final HomeBloc _homeBloc = HomeBloc();

  @override
  void initState() {
    _homeBloc.add(FetchLifeTimeSavings());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Theme.of(context).backgroundColor,
      body: BlocConsumer<HomeBloc, HomeState>(
        bloc: _homeBloc,
        listener: (context, state) {
          if (state is HomeLoaded) {
            _refreshController.refreshCompleted();
          }
          if (state is HomeError) {
            _refreshController.refreshCompleted();
            SnackBarHelper.showWarningSnackBar(context, state.developerMessage);
          }
        },
        builder: (context, state) {
          return Column(
            children: [
              Expanded(
                child: Container(
                  color: kSecondaryColor,
                  child: Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          SharedPrefsHelper.changeLoggedInValue(
                              newValue: false);
                          SharedPrefsHelper.clearUserValuesInMemory();
                          Navigator.of(context, rootNavigator: true)
                              .pushNamedAndRemoveUntil(
                                  LoginScreen.id, (route) => false);
                        },
                        child: Container(
                          alignment: Alignment.topRight,
                          padding: const EdgeInsets.only(right: 15, top: 30),
                          child: Icon(
                            Icons.power_settings_new,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Flexible(
                        child: Container(
//                          alignment: Alignment.center,
                          margin: EdgeInsets.all(25),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Image.asset('assets/avatar.png'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Padding(
                  padding: EdgeInsets.all(30),
                  child: Column(
                    children: [
                      Image.asset('assets/savings-gold.png'),
                      Text('Your patience and discipline is paying off!'),
                      SizedBox(height: 20),
                      Text(
                        'Lifetime Savings',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                        ),
                      ),
                      Text(
                        state is HomeLoaded
                            ? 'PKR ${state.lifetimeSavings}'
                            : 'N/A',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    _homeBloc.close();
    super.dispose();
  }
}
