import 'package:flutter/material.dart';
import 'package:oraan_test_app/screens/temp_screen.dart';

class PayNowTabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> _payNowTabKey;
  const PayNowTabNavigator(this._payNowTabKey);
  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: _payNowTabKey,
      initialRoute: TempScreen.id,
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case TempScreen.id:
            builder = (BuildContext _) => TempScreen();
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
