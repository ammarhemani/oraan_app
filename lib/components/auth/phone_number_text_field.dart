import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import '../../constants.dart';

class PhoneNumberTextField extends StatelessWidget {
  final bool autoValidate;
  final TextEditingController controller;
  final Function(String) onChanged;
  const PhoneNumberTextField(
      {this.onChanged, this.controller, this.autoValidate = false});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      inputFormatters: [
        LengthLimitingTextInputFormatter(11),
        WhitelistingTextInputFormatter.digitsOnly,
      ],
      validator: (value) => value.isEmpty ? 'Required' : null,
      autovalidate: autoValidate,
      onChanged: onChanged,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        labelText: 'Phone number',
        labelStyle: const TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
//        prefixText: '+92',
        border: OutlineInputBorder(),
        enabledBorder:
            OutlineInputBorder(borderSide: BorderSide(color: kOutlineColor)),
      ),
    );
  }
}
