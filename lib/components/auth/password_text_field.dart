import 'package:flutter/material.dart';

import '../../constants.dart';

class PasswordTextField extends StatelessWidget {
  final String placeholderText;
  final bool autoValidate;
  final TextEditingController controller;
  final bool obscurePassword;
  final Function onShowPasswordIconTap;
  const PasswordTextField(
      {@required this.obscurePassword,
      @required this.onShowPasswordIconTap,
      this.controller,
      this.autoValidate = false,
      this.placeholderText});
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: obscurePassword,
      validator: (value) => value.isEmpty ? 'Required' : null,
      autovalidate: autoValidate,
      decoration: InputDecoration(
        labelText: placeholderText ?? 'Password',
        labelStyle: const TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 16,
        ),
        hintText: 'xxxxx',
        border: OutlineInputBorder(),
        enabledBorder:
            OutlineInputBorder(borderSide: BorderSide(color: kOutlineColor)),
        suffixIcon: GestureDetector(
          onTap: onShowPasswordIconTap,
          child: _buildShowPasswordIcon(),
        ),
      ),
    );
  }

  Widget _buildShowPasswordIcon() {
    if (obscurePassword) {
      return const Padding(
        padding: EdgeInsets.all(8.0),
        child: ImageIcon(
          AssetImage('assets/show-password.png'),
        ),
      );
    } else {
      return const Padding(
        padding: EdgeInsets.all(8.0),
        child: ImageIcon(
          AssetImage('assets/hide-password.png'),
        ),
      );
    }
  }
}
