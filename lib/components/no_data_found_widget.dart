import 'package:flutter/material.dart';

class NoDataFoundWidget extends StatelessWidget {
  final EdgeInsets padding;
  const NoDataFoundWidget({this.padding});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.all(80.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(child: Image.asset('assets/no-data-found.png')),
          Text(
            'Oops..',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5),
          Text(
            "There's nothing here, yet.",
            style: TextStyle(
//                            fontWeight: FontWeight.bold,
              color: Colors.black54,
            ),
          ),
        ],
      ),
    );
  }
}
