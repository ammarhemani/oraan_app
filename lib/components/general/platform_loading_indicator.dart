import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformLoadingIndicator extends StatelessWidget {
  final bool overlay;
  const PlatformLoadingIndicator({Key key, this.overlay = true})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: overlay ? Colors.black.withOpacity(.2) : null,
      child: Center(
        child: Platform.isAndroid
            ? CircularProgressIndicator()
            : CupertinoActivityIndicator(),
      ),
    );
  }
}
