import 'package:flutter/material.dart';

class CustomErrorWidget extends StatelessWidget {
  final List<String> errorImages = [
//    'assets/error-1.png',
    'assets/error-2.png',
//    'assets/error-3.png',
//    'assets/error-4.png',
  ];
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(60.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset((errorImages..shuffle()).first),
          SizedBox(height: 20),
          Text(
            'Uh-oh..',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 5),
          Text(
            'Something went wrong..',
            style: TextStyle(color: Colors.black54),
          ),
        ],
      ),
    );
  }
}
