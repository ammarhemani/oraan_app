import 'package:flutter/material.dart';

import '../../constants.dart';

class CustomBadge<T> extends StatelessWidget {
  final T badgeCount;
  final Color color;
  final bool shrink;
  final double fontSize;
  final double height;
  final double width;
  const CustomBadge(
      {@required this.badgeCount,
      @required this.color,
      this.shrink = false,
      this.fontSize = 14,
      this.height,
      this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: shrink ? EdgeInsets.all(3) : EdgeInsets.all(6),
//                        width: 25,
//                        height: 25,
//      constraints: BoxConstraints(
//        minWidth: 300.0,
//        maxWidth: 300.0,
//        minHeight: 30.0,
//        maxHeight: 100.0,
//      ),
//      height: 35,
      height: height,
      width: width,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
        border: Border.all(color: Colors.white, width: 2),
        boxShadow: kContainerBoxShadow,
      ),
      child: FittedBox(
        child: Text(
          '$badgeCount',
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontSize: fontSize),
        ),
      ),
    );
  }
}
