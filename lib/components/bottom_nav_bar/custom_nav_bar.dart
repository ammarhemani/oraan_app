import 'package:flutter/material.dart';
import 'package:oraan_test_app/components/general/custom_badge.dart';
import '../../constants.dart';
import 'custom_nav_bar_item.dart';

class CustomNavBar extends StatelessWidget {
  final List<CustomNavBarItem> items;
  final int currentIndex;
  final int Function(int val) onTap;
  final Color selectedBackgroundColor;
  final Color selectedItemColor;
  final Color unselectedItemColor;
  final Color backgroundColor;
  final double fontSize;
  final double iconSize;

  const CustomNavBar(
      {Key key,
      @required this.items,
      @required this.currentIndex,
      @required this.onTap,
      this.backgroundColor = Colors.black,
      this.selectedBackgroundColor = Colors.white,
      this.selectedItemColor = Colors.black,
      this.iconSize = 24.0,
      this.fontSize = 11.0,
      this.unselectedItemColor = Colors.white})
      : assert(items.length > 1),
        assert(items.length <= 5),
        assert(currentIndex <= items.length),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
//      color: Colors.transparent,
      color: backgroundColor,
      elevation: 0,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.grey[300],
            ),
            height: 1,
          ),
          Container(
            padding: EdgeInsets.only(bottom: 8, top: 8),
            decoration: BoxDecoration(
//                borderRadius: BorderRadius.circular(8),
              color: backgroundColor,
            ),
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: items.map((f) {
                  return Expanded(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flexible(
                          child: InkWell(
                            onTap: () => onTap(items.indexOf(f)),
                            borderRadius: BorderRadius.circular(8),
                            child: Container(
                              width: MediaQuery.of(context).size.width *
                                  (100 / (items.length * 100)),
                              padding: EdgeInsets.all(4),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Stack(
                                    overflow: Overflow.visible,
                                    children: <Widget>[
                                      Icon(
                                        f.icon,
                                        color: currentIndex == items.indexOf(f)
                                            ? selectedItemColor
                                            : unselectedItemColor,
                                        size: iconSize,
                                      ),
                                      if (f.badgeCount != null &&
                                          f.badgeCount != 0)
                                        Positioned(
                                          top: -7,
                                          right: -15,
                                          child: CustomBadge<int>(
                                            badgeCount: f.badgeCount,
                                            color: kSecondaryColor,
                                          ),
                                        )
                                      else
                                        SizedBox.shrink(),
                                    ],
                                  ),
                                  Text(
                                    f.title,
                                    overflow: TextOverflow.ellipsis,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: currentIndex == items.indexOf(f)
                                            ? selectedItemColor
                                            : unselectedItemColor,
                                        fontSize: fontSize),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
