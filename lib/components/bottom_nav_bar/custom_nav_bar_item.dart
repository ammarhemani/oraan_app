import 'package:flutter/material.dart';

class CustomNavBarItem {
  final String title;
  final IconData icon;
  final int badgeCount;

  CustomNavBarItem({
    @required this.icon,
    @required this.title,
    this.badgeCount,
  });
}
