# Oraan Technical Test

## Getting Started

This project is a technical test for Oraan's Flutter development position.

[Official Documentation](https://docs.google.com/document/d/1z5B8cMAppwIs7XQKCzOsloQgk2gTcb5JpmvzKvQ8pXk/edit#)

## Screens include:

- Splash
- Login 
- Dashboard

## Figma Designs

[Figma Design File](https://www.figma.com/file/xRRawdsDdPoIXKRhq03wMS/Technical-test?node-id=281%3A8855)
[Assets](https://drive.google.com/drive/folders/1cdlkHdghO2imSnGzQEaAp37gcjSuf-Ga?usp=sharing)

## API Documentation

[APIs on Swagger](https://naya-oraan.herokuapp.com/swagger-ui.html)

## Example Credentials

![Credentials](https://drive.google.com/uc?export=download&id=1nMWdPQ_odNOxr2V7RUx85n0v0ZsDiZim)
